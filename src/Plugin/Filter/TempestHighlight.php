<?php

namespace Drupal\tempest_highlight\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Error as Error;
use Drupal\filter\Attribute\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\Plugin\FilterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tempest\Highlight\Highlighter;
use Tempest\Highlight\Themes\InlineTheme;

/**
 * Provides a filter to highlight code blocks using Tempest Highlight.
 *
 * This filter processes `<code>` blocks with a `language-` class
 * using the Tempest Highlight library to apply syntax highlighting.
 */
#[Filter(
  id: 'tempest_highlight',
  title: new TranslatableMarkup('Tempest Highlight'),
  type: FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
  description: new TranslatableMarkup('Process code blocks with Tempest Highlight'),
  weight: 110,
  settings: [
    'theme' => 'dark-plus',
  ],
)]
class TempestHighlight extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  const THEMES_BASE_PATH = DRUPAL_ROOT . '/../vendor/tempest/highlight/src/Themes/Css';

  const DEFAULT_THEME = 'dark-plus';

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ?FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileSystem = $fileSystem;
  }

  /**
   * Creates an instance of the plugin.
   *
   * This factory method retrieves the required dependencies from the service
   * container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param array $configuration
   *   A configuration array containing plugin instance information.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {

    $result = new FilterProcessResult($text);
    if (stristr($text, '<code') === FALSE || stristr($text, 'language-') === FALSE) {
      return $result;
    }

    $theme = $this->settings['theme'] ?? self::DEFAULT_THEME;

    try {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);
      $highlighter = (new Highlighter(new InlineTheme(self::THEMES_BASE_PATH . '/' . $theme . '.css')));
    }
    catch (\Exception $e) {
      Error::logException(\Drupal::logger('tempest_highlight'), $e);
      return $result;
    }

    foreach ($xpath->query('//code[starts-with(@class, "language-")]') as $node) {
      if (!$language = $this->getCodeLanguage($node->getAttribute('class'))) {
        continue;
      }

      $codeContent = $node->nodeValue;
      $codeHighlighted = $highlighter->parse($codeContent, $language);
      $newNode = $dom->createDocumentFragment();
      $newNode->appendXML("<code>{$codeHighlighted}</code>");
      $node->parentNode->replaceChild($newNode, $node);
    }

    $result->setProcessedText(Html::serialize($dom));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {

    $cssFiles = array_map(function ($file) {
      return str_replace('.css', '', $file->filename);
    }, $this->fileSystem->scanDirectory(self::THEMES_BASE_PATH, '/\.(css)$/'));

    ksort($cssFiles);

    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#options' => array_combine($cssFiles, $cssFiles),
      '#default_value' => $this->settings['theme'] ?? self::DEFAULT_THEME,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Highlight code-blocks using Tempest Highlight');
  }

  /**
   * Extracts the programming language from the class attribute.
   *
   * This method parses the class string to find a class that starts with
   * 'language-' and returns the language suffix.
   *
   * @param string $classes
   *   The space-separated class attribute string.
   *
   * @return string|null
   *   The extracted programming language, or NULL if no language is found.
   */
  private function getCodeLanguage($classes): ?string {
    foreach (explode(' ', $classes) as $class) {
      if (str_starts_with($class, 'language-')) {
        return substr($class, strlen('language-'));
      }
    }

    return NULL;
  }

}
